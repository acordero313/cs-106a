/*
 * File: Yahtzee.java
 * ------------------
 * This program will eventually play the Yahtzee game.
 */

import acm.io.*;
import acm.program.*;
import acm.util.*;
import java.util.*;

public class Yahtzee extends GraphicsProgram implements YahtzeeConstants {
	
	// Private instance variables
	private int nPlayers;
	private String[] playerNames;
	private YahtzeeDisplay display;
	private RandomGenerator rgen = new RandomGenerator();
	
	// 	Stores the most recent roll of dice
	private int[] diceRoll = new int[N_DICE]; 
	private int[][] scoreCard;
	
	public static void main(String[] args) {
		new Yahtzee().start(args);
	}
	
	public void run() {
		IODialog dialog = getDialog();
		this.nPlayers = dialog.readInt("Enter number of players");
		playerNames = new String[nPlayers];
		for (int i = 1; i <= nPlayers; i++) {
			playerNames[i - 1] = dialog.readLine("Enter name for player " + i);
		}
		display = new YahtzeeDisplay(getGCanvas(), playerNames);
		playGame();
	}

	private void playGame() {
		initializeScoreCard();
		
		for(int nTurns = 0; nTurns < N_SCORING_CATEGORIES; nTurns++) {
			for(int player = 1; player <= nPlayers; player++) {
				startFirstDiceRoll(player);
				subsequentDiceRolls(player);
				
				int category = getValidSelectedCategory(player);
				int score = calculateCategoryScore(category);	
				
				updateScore(category, player, score);
				displayScore(category, player, score);
			}
		}
		
		calculateTotalPlayerScores();
		pickWinner();
	}
	
	private void initializeScoreCard() {
		scoreCard = new int[N_CATEGORIES][nPlayers];
		for (int i = 0; i < N_CATEGORIES; i++) {
			for (int j = 0; j < nPlayers; j++) {
				scoreCard[i][j] = -1;
			}
		}
	}
	
	private void startFirstDiceRoll(int player) {
		display.printMessage(playerNames[player - 1] + 
				"'S turn! Click \"Roll Dice\" button to roll the dice" );
		display.waitForPlayerToClickRoll(player);
		
		for(int i = 0; i < N_DICE; i++) {
			diceRoll[i] = rgen.nextInt(1, 6);
		}
		
		display.displayDice(diceRoll);
	}
	
	private void subsequentDiceRolls(int player) {
		for(int i = 0; i < 2; i++) {
			
			display.printMessage("Select the dice you wish to re-roll and click \"Roll Again\".");
			display.waitForPlayerToSelectDice();
		
			for(int j = 0; j < N_DICE; j++) {
				if(display.isDieSelected(j)) {
					diceRoll[j] = rgen.nextInt(1, 6);
				}
			}
			
			display.displayDice(diceRoll);
		}
	}
	
	private boolean isValidCategory(int player, int category) {
		if (alreadyScored(player, category)) {
			return false;
		}
		
		if (category == UPPER_SCORE) {
			return false;
		}
		
		if (category == UPPER_BONUS) {
			return false;
		}
		
		if (category == LOWER_SCORE) {
			return false;
		}
		
		if (category == TOTAL) {
			return false;
		}
		
		return true;
	}
	
	private boolean alreadyScored(int player, int category) {
		return scoreCard[category - 1][player - 1] != -1; 	
	}
	
	private int getValidSelectedCategory(int player) {
		display.printMessage("Select category for this roll.");
		int category = display.waitForPlayerToSelectCategory();
		
		while(!isValidCategory(player, category)) {
			category = display.waitForPlayerToSelectCategory();
		}
		
		return category;
	}
	
	private int calculateCategoryScore(int category) {
		int score = 0;

		if (category >= ONES && category <= SIXES) {
			score = calculateUpperScore(category);
		}
		
		int[] diceCounts = countDice();
		
		if (category == THREE_OF_A_KIND && hasThreeOfAKind(diceCounts)) {
			score = sumAllDice();
		}
		
		if (category == FOUR_OF_A_KIND && hasFourOfAKind(diceCounts)) {
			score = sumAllDice();
		}
		
		if (category == FULL_HOUSE && hasFullHouse(diceCounts)) {
			score = 25;
		}
		
		if (category == SMALL_STRAIGHT && hasSmallStraight(diceCounts)) {
			score = 30;
		}
		
		if (category == LARGE_STRAIGHT && hasLargeStraight(diceCounts)) {
			score = 40;
		}
		
		if (category == YAHTZEE && hasYahtzee(diceCounts)) {
			score = 50;
		}
		
		if (category == CHANCE) {
			score = sumAllDice();
		}

		return score;
	}
	
	private boolean hasThreeOfAKind(int[] diceCounts) {
		for (int i = 0; i < diceCounts.length; i++) {
			if (diceCounts[i] >= 3) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean hasFourOfAKind(int[] diceCounts) {
		for (int i = 0; i < diceCounts.length; i++) {
			if(diceCounts[i] >= 4) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean hasFullHouse(int[] diceCounts) {
		int threeOfAKind = 0;
		for (int i = 0; i < diceCounts.length; i++) {
			if (diceCounts[i] >= 3) {
				threeOfAKind = i;
				break;
			}
		}
			
		if (threeOfAKind == 0) {
			return false;
		}
		
		for (int i = 0; i < diceCounts.length; i++) {
			if (i != threeOfAKind && diceCounts[i] >=2) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean hasSmallStraight(int[] diceCounts) {
		int smallStraight = 0;
		int count = 4;
		
		for (int potentialSmallStraight = 0; potentialSmallStraight < diceCounts.length - count;
				potentialSmallStraight++) {
			
			for (int offSet = 0; offSet <= 3; offSet++) {
				if(diceCounts[potentialSmallStraight + offSet] >= 1) {
					smallStraight += 1;
				}
			}
			
			if(smallStraight >= 4) {
				return true;	
				
			} else {
				smallStraight = 0;
			}
		}
		
		return false;
	}
	
	private boolean hasLargeStraight(int[] diceCounts) {
		int largeStraight = 0; 
		int count = 5;
		
		for (int potentialLargeStraightStart = 0; potentialLargeStraightStart <= diceCounts.length - count; 
				potentialLargeStraightStart++) {
			
			for (int offSet = 0; offSet <= 4; offSet++) {
				if(diceCounts[potentialLargeStraightStart + offSet] >= 1) {
					largeStraight += 1;
				}
			}
			
			if(largeStraight >= 5) {
				return true;
				
			} else {
				largeStraight = 0;
			}
		}
		
		return false;
	}
	
	private boolean hasYahtzee(int[] diceCounts) {
		for (int i = 0; i < diceCounts.length; i++) {
			if (diceCounts[i] == N_DICE) {
				return true;	
			}
		}
		
		return false;
	}
	
	private int sumAllDice() {	
		int sum = 0;
		for (int i = 0; i < N_DICE; i++) {
			sum += diceRoll[i];
		}
		
		return sum;
	}
	
	private int [] countDice() {
		int [] countDie = new int [6];
		int count = 0;
		
		for (int i = 0; i < N_DICE; i++) {
			count = diceRoll[i];
			countDie[count - 1] += 1; 
		}
		
		return countDie;
	}
	
	private int calculateUpperScore(int category) {
		int score = 0;
		for(int i = 0; i < N_DICE; i++) {
			if(diceRoll[i] == category) {
				score += category;
			}
		}
		
		return score;	
	}
	
	private void updateScore(int category, int player, int score) {
		scoreCard[category - 1][player - 1] = score;
	}
	
	private void displayScore(int category, int player, int score) {
		display.updateScorecard(category, player, score);
	}
	
	private void calculateTotalPlayerScores() {		
		for (int player = 1; player <= nPlayers; player++) {
			int upperScore = calculateTotalUpperScore(player);
			updateScore(UPPER_SCORE, player, upperScore);
			displayScore(UPPER_SCORE, player, upperScore);
			
			int upperBonus = calculateUpperBonus(player);
			updateScore(UPPER_BONUS, player, upperBonus);
			displayScore(UPPER_BONUS, player, upperBonus);
			
			int lowerScore = calculateTotalLowerScore(player);
			updateScore(LOWER_SCORE, player, lowerScore);
			displayScore(LOWER_SCORE, player, lowerScore);
			
			int total = upperScore + upperBonus + lowerScore;
			updateScore(TOTAL, player, total);
			displayScore(TOTAL, player, total);
		}
	}
	
	private int calculateTotalUpperScore(int player) {
		int upperScore = 0;
		
		for (int upperCategory = ONES; upperCategory <= SIXES; upperCategory++) {
			upperScore += scoreCard[upperCategory - 1][player - 1];
		}
		
		return upperScore;
	}
	
	private int calculateUpperBonus(int player) {
		int upperScore = scoreCard[UPPER_SCORE - 1][player - 1];
		if (upperScore > 63) {
			return 35;
		}
		
		return 0;
	}
	
	private int calculateTotalLowerScore(int player) {
		int lowerScore = 0;
		
		for(int lowerCategory = THREE_OF_A_KIND; lowerCategory <= CHANCE; lowerCategory++) {
			lowerScore += scoreCard[lowerCategory - 1][player - 1];
		}
		
		return lowerScore;
	}
	
	private void pickWinner() {
		int winner = 0;
		int score = 0;
		
		for(int i = 0; i < nPlayers; i++) {
			if(scoreCard[TOTAL - 1][i] > score) {
				score = scoreCard[TOTAL - 1][i];
				winner = i;
			}
		}
		
		display.printMessage("Congratulations," + playerNames[winner] + 
				"you're the winner with the total score of" + score);
	}
}
