/*
 * File: NameSurferEntry.java
 * --------------------------
 * This class represents a single entry in the database.  Each
 * NameSurferEntry contains a name and a list giving the popularity
 * of that name for each decade stretching back to 1900.
 */

import acm.util.*;
import java.util.*;

public class NameSurferEntry implements NameSurferConstants {
	
	private String name;
	private int[] ranking = new int[NDECADES];
	private String numberRanking;

/* Constructor: NameSurferEntry(line) */
/**
 * Creates a new NameSurferEntry from a data line as it appears
 * in the data file.  Each line begins with the name, which is
 * followed by integers giving the rank of that name for each
 * decade.
 */
	public NameSurferEntry(String line) {
		extractName(line);
		extractRanking(line);
		StringTokenizer st = new StringTokenizer(numberRanking, " ");
		
		for(int i = 0; st.hasMoreTokens(); i++) {
			int rank = Integer.parseInt(st.nextToken());
			this.ranking[i] = rank;
		}
	}
	
	private String extractName(String line) {
		int endOfName = line.indexOf(" ");
		this.name = line.substring(0, endOfName);
		return this.name;
	}
	
	private String extractRanking(String line) {
		int endOfName = line.indexOf(" ");
		this.numberRanking = line.substring(endOfName + 1);
		return this.numberRanking;
	}

/* Method: getName() */
/**
 * Returns the name associated with this entry.
 */
	public String getName() {
		
		return this.name;
	}

/* Method: getRank(decade) */
/**
 * Returns the rank associated with an entry for a particular
 * decade.  The decade value is an integer indicating how many
 * decades have passed since the first year in the database,
 * which is given by the constant START_DECADE.  If a name does
 * not appear in a decade, the rank value is 0.
 */
	public int getRank(int decade) {
		if (decade >= START_DECADE || decade <= END_DECADE);
		return this.ranking[decade];
	}

/* Method: toString() */
/**
 * Returns a string that makes it easy to see the value of a
 * NameSurferEntry.
 */
	public String toString() {
		
		return " " + this.name + ("[" + this.numberRanking + "]") + "";
	}
}
