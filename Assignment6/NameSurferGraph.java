/*
 * File: NameSurferGraph.java
 * ---------------------------
 * This class represents the canvas on which the graph of
 * names is drawn. This class is responsible for updating
 * (redrawing) the graphs whenever the list of entries changes or the window is resized.
 */

import acm.graphics.*;
import java.awt.event.*;
import java.util.*;
import java.awt.*;

public class NameSurferGraph extends GCanvas
	implements NameSurferConstants, ComponentListener {
	
	/* Implementation of the ComponentListener interface */
	public void componentHidden(ComponentEvent e) { }
	public void componentMoved(ComponentEvent e) { }
	public void componentResized(ComponentEvent e) { update(); }
	public void componentShown(ComponentEvent e) { }
	
	private ArrayList<NameSurferEntry> displayedData;
	
	/**
	* Updates the display image by deleting all the graphical objects
	* from the canvas and then reassembling the display according to
	* the list of entries.
	*/
	public void update() {
		removeAll();
		drawGraph();
		
		if(this.displayedData.size() > 0) {
			for (int i = 0; i < this.displayedData.size(); i++) {
				NameSurferEntry entryToBeDisplayed = this.displayedData.get(i);
				drawEntry(i, entryToBeDisplayed);
			}	
		}
	}

	/**
	* Creates a new NameSurferGraph object that displays the data.
	*/
	public NameSurferGraph() {
		addComponentListener(this);
		this.displayedData = new ArrayList<NameSurferEntry>();
	}
	
	/**
	* Clears the list of name surfer entries stored inside this class.
	*/
	public void clear() {
		this.displayedData.clear();
	}
	
	/**
	* Adds a new NameSurferEntry to the list of entries on the display.
	* Note that this method does not actually draw the graph, but
	* simply stores the entry; the graph is drawn by calling update.
	*/
	public void addEntry(NameSurferEntry entry) {
		this.displayedData.add(entry);
	}
	
	private void drawGraph() {
		drawVerticalLines();
		drawHorizontalLines();
		drawDecadesLabel();
	}
	
	private void drawVerticalLines() {
		double startY = 0;
		double endY = getHeight();
		
		for(int i = 0; i < NDECADES; i++) {
			double startX = i * (getWidth() / NDECADES);
			double endX = startX;
			GLine verticalLine = drawLine(startX, startY, endX, endY);
			add(verticalLine);
		}		
	}
	
	private void drawHorizontalLines() {
		double startX = 0;
		double endX = getWidth();
		double startFromBottomY = getHeight() - GRAPH_MARGIN_SIZE;
		
		GLine bottomHorizontalLine = drawLine(startX, startFromBottomY, endX, startFromBottomY);
		add(bottomHorizontalLine);
		
		double startFromTopY = GRAPH_MARGIN_SIZE;
		GLine topHorizontalLine = drawLine(startX, startFromTopY, endX, startFromTopY);
		add(topHorizontalLine);
	}
	
	private GLine drawLine(double startX, double startY, double endX, double endY) {
		GLine line = new GLine (startX, startY, endX, endY);
		add(line);
		
		return line;	
	}
	
	private void drawDecadesLabel() {
		double startY = getHeight() - (GRAPH_MARGIN_SIZE / 2);

		for (int i = 0; i < NDECADES; i++) {
			int relativeDecade = 10 * i;
			String decadeLabel = Integer.toString(START_DECADE + relativeDecade);
			
			double startX = i * (getWidth() / NDECADES);			
			GLabel displayDecadeLabel = new GLabel(decadeLabel, startX, startY);
			add(displayDecadeLabel);
		}
	}
		
	private void drawEntry(int entryColor, NameSurferEntry entry) {
		double columnWidth = getWidth() / NDECADES;
		
		for (int marker = 0; marker < NDECADES - 1; marker++) {
			
			GLine graphLine = drawLine(columnWidth * marker, verticalValue(entry.getRank(marker)), 
					columnWidth * (marker + 1), verticalValue(entry.getRank(marker + 1)));
			
			colorSelectForGraphLine(entryColor, graphLine);
			add(graphLine);
		}	
		
		for (int marker = 0; marker < NDECADES; marker++) {
			
			GLabel rankLabel = drawRankLabel(entry, marker);
			colorSelectForLabel(entryColor, rankLabel);
			add(rankLabel, columnWidth * marker, verticalValue(entry.getRank(marker)));
		}
	}
	
	private GLabel drawRankLabel(NameSurferEntry entry, int marker) {
		double columnWidth = getWidth() / NDECADES;
		
		String entryLabel = "";
			
		if(entry.getRank(marker) != 0) {
			entryLabel = entry.getName() + " " + entry.getRank(marker);	
		}
			
		if(entry.getRank(marker) == 0) {
			entryLabel = entry.getName() + " *";
		}
			
		GLabel nameLabel = new GLabel(entryLabel, columnWidth * marker, verticalValue(entry.getRank(marker)));
		
		return nameLabel;
	}
	
	private void colorSelectForLabel(int entryColor, GLabel rankLabel) {
		entryColor %= 4;
		
		if(entryColor == 0){
			rankLabel.setColor(Color.RED);
		}
		
		if(entryColor == 1){
			rankLabel.setColor(Color.BLUE);
		}
		
		if(entryColor == 2){
			rankLabel.setColor(Color.ORANGE);
		}
		
		if(entryColor == 3){
			rankLabel.setColor(Color.MAGENTA);
		}	
	}
	
	private void colorSelectForGraphLine(int entryColor, GLine graphLine) {
		entryColor %= 4;
		
		if(entryColor == 0){
			graphLine.setColor(Color.RED);
		}
		
		if(entryColor == 1){
			graphLine.setColor(Color.BLUE);
		}
		
		if(entryColor == 2){
			graphLine.setColor(Color.ORANGE);
		}
		
		if(entryColor == 3){
			graphLine.setColor(Color.MAGENTA);
		}	
	}
	
	private double verticalValue(int ranking) {
		double rank = ranking;
		double graphHeight = getHeight() - (GRAPH_MARGIN_SIZE * 2);
		
		if(rank != 0) {
			rank = ((graphHeight / MAX_RANK) * rank) + GRAPH_MARGIN_SIZE;
		}  
		
		if(rank == 0) {
			rank = getHeight() - GRAPH_MARGIN_SIZE;
		}
		
		return rank;
	}
}
