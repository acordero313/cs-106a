/*
 * File: NameSurfer.java
 * ---------------------
 * When it is finished, this program will implements the viewer for
 * the baby-name database described in the assignment handout.
 */

import acm.program.*;
import java.awt.event.*;
import javax.swing.*;

public class NameSurfer extends Program implements NameSurferConstants {
	
	private NameSurferGraph graph;
	private NameSurferDataBase nameDataBase;
	
	private JTextField nameField;
	private JButton Graph;
	private JButton Clear;
	private JLabel Name;
	
/**
 * This method has the responsibility for reading in the data base
 * and initializing the interactors at the bottom of the window.
 */
	public void init() {
		graph = new NameSurferGraph();
		add(graph);
		
		nameField = new JTextField(20);
		Name = new JLabel ("Name");
		add(Name, SOUTH);
		add(nameField, SOUTH);
		nameField.addActionListener(this);
		
		Graph = new JButton("Graph");
		Clear = new JButton("Clear");
		
		add(Graph, SOUTH);
		add(Clear, SOUTH);
		
		addActionListeners();
		
		nameDataBase = new NameSurferDataBase(NAMES_DATA_FILE);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Clear")) {
			graph.clear();
			graph.update();
			
		} else {
		
			String enteredName = nameField.getText();
			NameSurferEntry rankings = nameDataBase.findEntry(enteredName);
			if(rankings != null) {
				graph.addEntry(rankings);
				graph.update();
			}
		}
	}
}
