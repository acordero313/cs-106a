/*
 * File: Target.java
 * Name: 
 * Section Leader: 
 * -----------------
 * This file is the starter file for the Target problem.
 */

import acm.graphics.*;
import acm.program.*;
import java.awt.*;


public class Target extends GraphicsProgram {	
	
	private static final double OUTER_CIRCLE_RADIUS = 72;
	private static final double MIDDLE_CIRCLE_RADIUS = 72 * 65 /100;
	private static final double INNER_CIRCLE_RADIUS = 72 * 3 / 10;
	
	
	public void run() {
		placeOuterCircle();
		placeMiddleCircle();
		placeInnerMostCircle();
	}
	
	private void placeOuterCircle() {
		int radius = 72;
		int x = getWidth()/2 - radius/2;
		int y = getHeight()/2 - radius/2;
		GOval OuterMostCircle = new GOval (x, y, radius, radius);
		OuterMostCircle.setColor(Color.RED);
		OuterMostCircle.setFilled(true);
		OuterMostCircle.setFillColor(Color.RED);
		add(OuterMostCircle);
	}
	
	private void placeMiddleCircle() {
		double radius = 72 * 65 /100;
		double x = getWidth()/2 - radius/2;
		double y = getHeight()/2 - radius/2;
		GOval middleCircle = new GOval (x, y, radius, radius);
		middleCircle.setColor(Color.WHITE);
		middleCircle.setFilled(true);
		middleCircle.setFillColor(Color.WHITE);
		add(middleCircle);
	}
	
	private void placeInnerMostCircle() {
		double radius = 72 * 3 / 10;
		double x = getWidth()/2 - radius/2;
		double y = getHeight()/2 - radius/2;
		GOval innerMostCircle = new GOval (x,y, radius, radius);
		innerMostCircle.setColor(Color.RED);
		innerMostCircle.setFilled(true);
		innerMostCircle.setFillColor(Color.RED);
		add(innerMostCircle);
	}
}
