/*
 * File: ProgramHierarchy.java
 * Name: 
 * Section Leader: 
 * ---------------------------
 * This file is the starter file for the ProgramHierarchy problem.
 */

import acm.graphics.*;
import acm.program.*;
import java.awt.*;

public class ProgramHierarchy extends GraphicsProgram {	
	
	private static final int FRAME_WIDTH = 200;
	private static final int FRAME_HEIGHT = 75;
	
	private static final int BOX_WIDTH = 200;
	private static final int BOX_HEIGHT = 75;
	
	
	public void run() {
		drawHeierarchy();
	}
	
	public void drawHeierarchy() {
		double programBoxX = (getWidth() - BOX_WIDTH) / 2;
		double programBoxY = (getHeight() - BOX_HEIGHT) / 2;
		GRect programBox = drawBox(programBoxX, programBoxY, "Program");
		
		double consoleBoxX = programBoxX;
		double consoleBoxY = programBoxY + (BOX_HEIGHT * 2);
		GRect consoleBox = drawBox(consoleBoxX, consoleBoxY, "ConsoleProgram");
		
		double graphicsBoxX = consoleBoxX - (BOX_WIDTH * 1.5);
		double graphicsBoxY = consoleBoxY;
		GRect graphicsBox = drawBox(graphicsBoxX, graphicsBoxY, "GraphicsProgram");
		
		double dialogBoxX = consoleBoxX + (BOX_WIDTH * 1.5);
		double dialogBoxY = consoleBoxY;
		GRect dialogBox = drawBox(dialogBoxX, dialogBoxY, "DialogProgram");
		
		drawLine(programBox, consoleBox);
		drawLine(programBox, graphicsBox);
		drawLine(programBox, dialogBox);
	}
	
	private GRect drawBox(double boxX, double boxY, String labelContents) {
		GRect box = new GRect(boxX, boxY, BOX_WIDTH, BOX_HEIGHT);
		add(box);
		
		GLabel label = new GLabel(labelContents);
		centerLabel(box, label);
		add(label);
		
		return box;
	}
	
	private void centerLabel(GRect box, GLabel label) {
		double labelOffsetX = (box.getWidth() - label.getWidth()) / 2;
		double labelOffsetY = (box.getHeight() - label.getAscent()) / 2;
		
		double labelX = box.getX() + labelOffsetX;
		double labelY = box.getY() + labelOffsetY + label.getAscent();
		
		label.setLocation(labelX, labelY);
	}
	
	private void drawLine(GRect topBox, GRect bottomBox) {
		double topBoxBottomMiddleX = topBox.getX() + (topBox.getWidth() / 2);
		double topBoxBottomMiddleY = topBox.getY() + topBox.getHeight();
		
		double bottomBoxTopMiddleX = bottomBox.getX() + (bottomBox.getWidth() / 2);
		double bottomBoxTopMiddleY = bottomBox.getY();
		
		GLine line = new GLine(topBoxBottomMiddleX, topBoxBottomMiddleY,
				bottomBoxTopMiddleX, bottomBoxTopMiddleY);
		add(line);
	}
}

