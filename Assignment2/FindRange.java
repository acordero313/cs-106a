/*
 * File: FindRange.java
 * Name: 
 * Section Leader: 
 * --------------------
 * This file is the starter file for the FindRange problem.
 */

import acm.program.*;

public class FindRange extends ConsoleProgram {
	
	private static final int SENTINEL_VALUE = 0;
	
	public void run() {
		findNumberRange();
	}
	
	private int getFirstNonSentinelValue() {
		println("This program finds the largest and smallest numbers.");
		
		int number = readInt("? ");
		
		while (number == SENTINEL_VALUE) {
			println("No value has been entered");
			number = readInt("? ");
		}
		
		return number;
	}
	
	private void runningMinMax(int startingValue) {
		int smallest = startingValue;
		int largest = startingValue;
		
		int number = readInt("? ");
		
		while (number != SENTINEL_VALUE) {
			if (number < smallest) {
				smallest = number;
			}
			
			if (number > largest) {
				largest = number;
			}
			
			number = readInt("? ");
		}
		
		println("smallest: " + smallest);
		println("largest: " + largest);	
	}
	
	private void findNumberRange() {
		int startingValue = getFirstNonSentinelValue();
		runningMinMax(startingValue);
	}
}
	