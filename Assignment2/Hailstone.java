/*
 * File: Hailstone.java
 * Name: 
 * Section Leader: 
 * --------------------
 * This file is the starter file for the Hailstone problem.
 * 
 * Pick some positive integer and call it n.
 * If n is even, divide it by two.
 * If n is odd, multiply it by three and add one.
 * Continue this process until n is equal to one.
 */

import acm.program.*;

public class Hailstone extends ConsoleProgram {
	public void run() {
		int stepsCounted = 0;
		int currentNumber = readInt ("Enter a number: ");
		
		while (currentNumber != 1) {
			stepsCounted++;
			
			if (currentNumber % 2 == 1) {
				int nextNumber = 3 * currentNumber + 1;
				println(currentNumber + " is odd, so I make 3n + 1: " + nextNumber);
				currentNumber = nextNumber;
				continue;
			}
			
			int nextNumber = currentNumber / 2;
			println(currentNumber + " is even, so I take half: " + nextNumber);
			currentNumber = nextNumber;
			
			
			if (currentNumber % 2 == 1) {
				int tempNumber = 3 * currentNumber + 1;
				println(currentNumber + " is odd, so I make 3n + 1: " + nextNumber);
				currentNumber = nextNumber;
			} else {
				int tempNumber = currentNumber / 2;
				println(currentNumber + " is even, so I take half: " + nextNumber);
				currentNumber = nextNumber;
			}
			stepsCounted++;
		}
		
		println("The processs took " + stepsCounted + " steps to reach 1.");
	}
}
