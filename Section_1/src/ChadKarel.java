/*
 * File: ChadKarel.java
 * ---------------------
 *
 */

import stanford.karel.*;

public class ChadKarel extends SuperKarel {
	
	public void run() {
		if (noBeepersPresent()) {
			removeChadFromCastedVote();
		}
		
		while (frontIsClear()) {
			move();
			if (noBeepersPresent()) {
				removeChadFromCastedVote();
			}	
		}
	}
	
	
	/*
	 * Pre-Condition: Must be facing east while in the middle of the ballot in order to execute.
	 * Post-Condition: Karel will check both the top and bottom of a single row of a ballot for any remaining beepers. Returning back to its original state, middle of the row, facing east.
	 */

	private void removeChadFromCastedVote() {
		removeChadFromBottom();
		removeChadFromTop();
	}
		
	private void removeChadFromBottom() {
		turnRight();
		move();
		pickUpAllBeepers();
		turnAround();
		move();
		turnRight();
	}
	
	private void removeChadFromTop() {
		turnLeft();
		move();
		pickUpAllBeepers();
		turnAround();
		move();
		turnLeft();
	}
	
	private void pickUpAllBeepers() {
		while (beepersPresent()) {
			pickBeeper();
		}
	}
}
