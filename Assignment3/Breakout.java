/*
 * File: Breakout.java
 * -------------------
 * Name:
 * Section Leader:
 * 
 * This file will eventually implement the game of Breakout.
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class Breakout extends GraphicsProgram {

/** Width and height of application window in pixels */
	public static final int APPLICATION_WIDTH = 400;
	public static final int APPLICATION_HEIGHT = 600;

/** Dimensions of game board (usually the same) */
	private static final int WIDTH = APPLICATION_WIDTH;
	private static final int HEIGHT = APPLICATION_HEIGHT;

/** Dimensions of the paddle */
	private static final int PADDLE_WIDTH = 60;
	private static final int PADDLE_HEIGHT = 10;

/** Offset of the paddle up from the bottom */
	private static final int PADDLE_Y_OFFSET = 30;

/** Number of bricks per row */
	private static final int NBRICKS_PER_ROW = 10;

/** Number of rows of bricks */
	private static final int NBRICK_ROWS = 10;

/** Separation between bricks */
	private static final int BRICK_SEP = 4;

/** Width of a brick */
	private static final int BRICK_WIDTH =
	  (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

/** Height of a brick */
	private static final int BRICK_HEIGHT = 8;

/** Radius of the ball in pixels */
	private static final int BALL_RADIUS = 10;

/** Offset of the top brick row from the top */
	private static final int BRICK_Y_OFFSET = 70;

/** Number of turns */
	private static final int NTURNS = 3;
	
	private GRect brick;

	private GRect paddle;
	
//	Will keep track the number of times the player has lost a game
	private int turn = 0;
	
// 	Total number of bricks that determines when a player had won the game 
	private int brickCount = 100;
	
//	Keeps track of the velocity of the wall
	private double vx, vy;
	
//	Random number generator for vx
	private RandomGenerator rgen = RandomGenerator.getInstance();
	
	private GOval ball;
	
/* Method: run() */
/** Runs the Breakout program. */
	public void run() {

		addMouseListeners();
		
		setUpGame();
		playGame();
	}
	
	// Method that will prevent the paddle from going beyond the application width
	
	public void mouseMoved(MouseEvent e) {
		double paddleMouseOffset = PADDLE_WIDTH / 2;
		
		double movedPaddleX = e.getX() - paddleMouseOffset;
		double movedPaddleY = paddle.getY();
		
		if (movedPaddleX < 0) {
			movedPaddleX = 0;
		}
		
		if ((movedPaddleX + PADDLE_WIDTH) > APPLICATION_WIDTH) {
			movedPaddleX = APPLICATION_WIDTH - PADDLE_WIDTH;
		}
		
		paddle.setLocation(movedPaddleX, movedPaddleY);
	}
	
	private void setUpGame () {
		buildBricks();
		buildPaddle();
		buildBall();
	}
	
	// Method that will build all of the necessary bricks for the game
	
	private void buildBricks() {
		for (int row = 0; row < NBRICK_ROWS; row++) {
			for (int col = 0; col < NBRICKS_PER_ROW; col++) {
				
				double xStart = col * (BRICK_WIDTH + BRICK_SEP);
				double yStart = BRICK_Y_OFFSET + (BRICK_HEIGHT + BRICK_SEP) * row;
				
				GRect brick = new GRect (xStart, yStart, BRICK_WIDTH, BRICK_HEIGHT);
				brick.setFilled(true);
				add(brick);
				
				this.brick = brick;
				
				switch (row) {
				case 0: 
					brick.setColor(Color.red);
					break;
				case 1: 
					brick.setColor(Color.red);
					break;
				case 2: 
					brick.setColor(Color.orange);
					break;
				case 3: 
					brick.setColor(Color.orange); 
					break;
				case 4: 
					brick.setColor(Color.yellow); 
					break;
				case 5: 
					brick.setColor(Color.yellow); 
					break;
				case 6: 
					brick.setColor(Color.green); 
					break;
				case 7: 
					brick.setColor(Color.green); 
					break;
				case 8: 
					brick.setColor(Color.blue); 
					break;
				case 9: 
					brick.setColor(Color.blue);
					break;
				}
			}
		}
	}
	
	private void buildPaddle() {
		double xStart = (APPLICATION_WIDTH / 2) - PADDLE_WIDTH / 2; 
		double yStart = (APPLICATION_HEIGHT - PADDLE_Y_OFFSET) - PADDLE_HEIGHT;
		
		GRect paddle = new GRect (xStart, yStart, PADDLE_WIDTH, PADDLE_HEIGHT);	
		paddle.setFilled(true);
		paddle.setColor(Color.black);
		add(paddle);
		
		this.paddle = paddle;
	}
	
	private void buildBall() {
		double xStart = (APPLICATION_WIDTH / 2) - BALL_RADIUS;
		double yStart = (APPLICATION_HEIGHT / 2) + BALL_RADIUS;
		
		GOval ball = new GOval (xStart, yStart, BALL_RADIUS * 2, BALL_RADIUS * 2);
		ball.setFilled(true);
		ball.setFillColor(Color.black);
		add(ball);
		
		this.ball = ball;
	}
	
	private void playGame() {
		initializeVelocity();
		
		while (turn < NTURNS && brickCount > 0) {
			
			moveBallOnce();
			
			if (isBallTouchingLeftWall()) {
				vx = -vx;	
			}
			
			if (isBallTouchingRightWall()) {
				vx = -vx;
			}

			if (isBallTouchingCeiling()) {
				vy = -vy;
			}	
			
			if (isBallTouchingFloor()) {
				turn++;
				centerBall();
			}
			
			GObject collider = getCollidingObject();
			if (collider == paddle) {
				vy = -vy;

			} else if (collider != null) {
				vy = -vy;
				removeBrick(collider);
			}
		}
	}
	
	private void removeBrick(GObject brick) {
		remove(foo);
		brickCount--;
	}
	
	private boolean isBallTouchingLeftWall() {
		return ball.getX() < 0;
	}
	
	private boolean isBallTouchingRightWall() {
		return ball.getX() + BALL_RADIUS > APPLICATION_WIDTH - BALL_RADIUS;
	}
	
	private boolean isBallTouchingCeiling() {
		return ball.getY() < 0;	
	}
	
	private boolean isBallTouchingFloor() {
		return ball.getY() > APPLICATION_HEIGHT;
	}
	
	private void initializeVelocity() {
		vy = 3.0;
		vx = rgen.nextDouble(1.0, 3.0);
		
		if (rgen.nextBoolean(0.5)) {
			vx = -vx;
		}
	}
	
	private void moveBallOnce() {
		double newBallX = ball.getX() + vx;
		double newBallY = ball.getY() + vy;
		
		ball.setLocation(newBallX, newBallY);
		pause(20);
	}
	
	private void centerBall() {
		double xStart = (APPLICATION_WIDTH / 2) - BALL_RADIUS;
		double yStart = (APPLICATION_HEIGHT / 2) + BALL_RADIUS;
		
		ball.setLocation(xStart, yStart);
	}
	
	// Method that returns back the colliding object 
	
	private GObject getCollidingObject() {
		double leftX = ball.getX();
		double rightX = ball.getX() + (2 * BALL_RADIUS);

		double topY = ball.getY();
		double bottomY = ball.getY() + (2 * BALL_RADIUS);

		GObject topLeftObject = getElementAt(leftX, topY);
		if(topLeftObject != null) {
			return topLeftObject;
		}

		GObject topRightObject = getElementAt(rightX, topY);
		if(topRightObject != null) {
			return topRightObject;
		}

		GObject bottomRightObject = getElementAt(rightX, bottomY);
		if(bottomtRightObject != null) {
			return bottomRightObject;
		}

		GObject bottomLeftObject = getElementAt(leftX, bottomY);
		if(bottomLeftObject != null) {
			return bottomLeftObject;
		}

		return null;

	}
}
	