/*
 * File: StoneMasonKarel.java
 * --------------------------
 * The StoneMasonKarel subclass as it appears here does nothing.
 * When you finish writing it, it should solve the "repair the quad"
 * problem from Assignment 1.  In addition to editing the program,
 * you should be sure to edit this comment so that it no longer
 * indicates that the program does nothing.
 */

import stanford.karel.*;

public class StoneMasonKarel extends SuperKarel {

	
	public void run () {
		fillColumn();
		if (frontIsClear()) {
			repairRemainingColumns();
		}
	}
	
	/*
	 * Pre-Condition: Must be at the bottom of the column, facing east, and will then re-orient north in order to execute
	 * Post-Condition: Will be at the top of the column facing north
	 */
	
	private void reconstructColumn () {
		if (noBeepersPresent()) {
			putBeeper();
		}
		while (frontIsClear()) {
			move();
			if (noBeepersPresent()) {
				putBeeper();
			}
		}
	}
	
	/*
	 * Pre-Condition: Must be at the bottom of the column facing east
	 * Post-Condition: Will end up at the bottom of the column facing east
	 */
	
	private void fillColumn() {
		turnLeft();
		reconstructColumn();
		turnAround();
		moveToBottomOfColumn();
		turnLeft();
	}
	
	/*
	 * Pre-Condition: Must be at the top of the column facing south
	 * Post-Condition: Will end up at the bottom of the column, facing south
	 */

	private void moveToBottomOfColumn() {
		while (frontIsClear()) {
			move();	
		}
	}
	
	/* Pre-Condtion: Must be at the bottom of the column facing east
	 * Post-Condition: If the front is clear, will move over to the following column
	 */

	private void repairRemainingColumns() {
		move();
		move();
		move();
		move();		
		fillColumn();			
		if (frontIsClear()) {
			repairRemainingColumns();
		}
	}
}	