/*
 * File: CheckerboardKarel.java
 * ----------------------------
 * When you finish writing it, the CheckerboardKarel class should draw
 * a checkerboard using beepers, as described in Assignment 1.  You
 * should make sure that your program works for all of the sample
 * worlds supplied in the starter folder.
 * 
 * 1. checker row
 * 2. Reposition to face west
 * 3. Reposition to face east
 */

import stanford.karel.*;

public class CheckerboardKarel extends SuperKarel {
	
	
	public void run () {
		completeRowFacingEast();
	}
	
	private void completeRow() {
		while (frontIsClear()) {
			if (beepersPresent()) {
				move();
			} else {
				move();
				putBeeper();
			}
		}
	}
		
	private void completeRowFacingEast() {
		completeRow();
		turnLeft();
		if (frontIsClear()){
			if(beepersPresent()){
				move();
				turnLeft();
				completeRowFacingWest();
			} else {
				move();
				turnLeft();
				putBeeper();
				completeRowFacingWest();
			}
		}
	}
	
	private void completeRowFacingWest() {
		completeRow();
		turnRight();
		if (frontIsClear()) {
			if(beepersPresent()){
				move();
				turnRight();
				completeRowFacingEast();
			} else {
				move();
				turnRight();
				putBeeper();
				completeRowFacingEast();
			}
		}
	}
}