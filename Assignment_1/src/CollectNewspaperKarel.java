/*
 * File: CollectNewspaperKarel.java
 * --------------------------------
 * At present, the CollectNewspaperKarel subclass does nothing.
 * Your job in the assignment is to add the necessary code to
 * instruct Karel to walk to the door of its house, pick up the
 * newspaper (represented by a beeper, of course), and then return
 * to its initial position in the upper left corner of the house.
 * 
 * 1. Move to newspaper 
 * 2. Pick up newspaper
 * 3. Return to start 
 */

import stanford.karel.*;

public class CollectNewspaperKarel extends SuperKarel {

	
	public void run() {
		moveToNewspaper();
		pickUpNewspaper();
		returnToStart();
		putBeeperDown();
	}
	
	/* Pre-Condition: Karel starts off at the northwest corner of the house, facing east
	 * Post-Condition: Karel is facing east, on top of the newspaper 
	 */
	
	private void moveToNewspaper () {
		move();
		move();
		turnRight();
		move();
		turnLeft();
		move();
	}
	
	/* Pre-Condition: Karel is facing east, on top of the newspaper
	 * Post-Condition: Karel picks up the newspaper, still facing east
	 */
	
	private void pickUpNewspaper () {
		pickBeeper();
	}
	
	/* Pre-Condition: Karel picks up the newspaper, still facing east
	 * Post-Condition: Karel returns to the northwest corner of the house, ends facing east. 
	 */
	
	private void returnToStart () {
		turnAround () ;
		move();
		move();
		move();
		turnRight();
		move();
		turnRight();
	}
	
	/* Pre-Condition: Must be at the northwest corner of the house, facing east
	 * Post-Condition: Karel drops off newspaper at the northwest corner of the house
	 */
	
	private void putBeeperDown () {
		putBeeper();
	}
}