/*
 * File: HangmanCanvas.java
 * ------------------------
 * This file keeps track of the Hangman display.
 */

import acm.graphics.*;
import acm.program.*;
import java.awt.*;

public class HangmanCanvas extends GCanvas {
	
	/* Constants for the simple version of the picture (in pixels) */
	private static final int SCAFFOLD_HEIGHT = 360;
	private static final int BEAM_LENGTH = 144;
	private static final int ROPE_LENGTH = 18;
	private static final int HEAD_RADIUS = 36;
	private static final int BODY_LENGTH = 144;
	private static final int ARM_OFFSET_FROM_HEAD = 28;
	private static final int UPPER_ARM_LENGTH = 72;
	private static final int LOWER_ARM_LENGTH = 44;
	private static final int HIP_WIDTH = 36;
	private static final int LEG_LENGTH = 108;
	private static final int FOOT_LENGTH = 28;
	
	private String incorrectGuess = "";
	
	private GLabel unGuessedWord;
	private GLabel incorrectLetters;

/** Resets the display so that only the scaffold appears */
	public void reset() {
		drawScaffold();
	}

/**
 * Updates the word on the screen to correspond to the current
 * state of the game.  The argument string shows what letters have
 * been guessed so far; unguessed letters are indicated by hyphens.
 */
	public void displayWord(String word) {
		double x = getWidth() / 4;
		double y = getHeight() - (HEAD_RADIUS * 2);
		this.unGuessedWord = new GLabel(word, x, y);
		this.unGuessedWord.setFont("TIMES_ROMAN-24");

		if (getElementAt(x, y) != null) {
			remove(getElementAt(x, y));
		}
	        
		add(this.unGuessedWord);   
	}

/**
 * Updates the display to correspond to an incorrect guess by the
 * user.  Calling this method causes the next body part to appear
 * on the scaffold and adds the letter to the list of incorrect
 * guesses that appears at the bottom of the window.
 */
	public void noteIncorrectGuess(char letter) {
		double x = getWidth() / 4;
        double y = getHeight() - HEAD_RADIUS;
        incorrectGuess += letter;
        this.incorrectLetters = new GLabel(incorrectGuess, x, y);
        this.incorrectLetters.setFont("TIMES_ROMAN-30");

        if(getElementAt(x, y) != null) {
            remove(getElementAt(x, y));
        }
        
        add(this.incorrectLetters);

        switch(incorrectGuess.length()) {
        
        	case 1: 
        		drawHead(); 
        		break;
        	case 2: 
        		drawBody(); 
        		break;
        	case 3: 
        		drawLeftArm(); 
        		break; 
        	case 4: 
        		drawRightArm(); 
        		break;
        	case 5: 
        		drawLeftLeg(); 
        		break;
        	case 6: 
        		drawRightLeg(); 
        		break;
        	case 7: 
        		drawLeftFoot(); 
        		break;
        	case 8: 
        		drawRightFoot(); 
        		break;
        }
	}
	
	private void drawScaffold() {
		double scaffoldTopX = (getWidth() - BEAM_LENGTH) / 2 ;
		double scaffoldTopY = getHeight() / 2 - BODY_LENGTH - (HEAD_RADIUS * 2) - ROPE_LENGTH;
		double scaffoldBottomY = scaffoldTopY + SCAFFOLD_HEIGHT;
        
		GLine scaffold= drawLine(scaffoldTopX, scaffoldTopY, scaffoldTopX, scaffoldBottomY);
		add(scaffold);
        
		double beamRightX = scaffoldTopX + BEAM_LENGTH;
		GLine beam = drawLine(scaffoldTopX, scaffoldTopY, beamRightX, scaffoldTopY);
		add(beam);

		double ropeBottomY = scaffoldTopY + ROPE_LENGTH;
		GLine rope = drawLine(beamRightX, scaffoldTopY, beamRightX, ropeBottomY);
		add(rope);
	}
	
	private void drawHead() {
		double startX = getWidth() / 2 - UPPER_ARM_LENGTH + BEAM_LENGTH - HEAD_RADIUS; 
		double startY = getHeight() / 2 - (BODY_LENGTH *2) + (HEAD_RADIUS *2);
		
		GOval head = new GOval (startX, startY, HEAD_RADIUS * 2, HEAD_RADIUS * 2);
		add(head);
	}
	
	private void drawBody() {
		double startX = getWidth() / 2 + UPPER_ARM_LENGTH + HEAD_RADIUS;
		double startY = getHeight() / 2 - BODY_LENGTH;
		
		GLine body = drawLine(startX - HEAD_RADIUS, startY, startX - HEAD_RADIUS , startY + BODY_LENGTH);
		add(body);
	}
	
	private GLine drawLine(double startX, double endY, double startXTwo, double endYTwo) {
		GLine line = new GLine( startX, endY, startXTwo, endYTwo);
		add(line);
		
		return line;
	}
	
	private void drawLeftArm() {
		double armStartX = getWidth() / 2 + UPPER_ARM_LENGTH/2 + HEAD_RADIUS;
		double armEndX = getWidth() / 2 + UPPER_ARM_LENGTH/2 + HEAD_RADIUS - UPPER_ARM_LENGTH;
		double upperArmHeightY = getHeight()/2 - BODY_LENGTH + ARM_OFFSET_FROM_HEAD;
	     
		GLine upperLeftArm = drawLine(armStartX, upperArmHeightY, armEndX, upperArmHeightY);
		add(upperLeftArm);
	     
		double lowerArmEndY = upperArmHeightY + LOWER_ARM_LENGTH;
		GLine lowerLeftArm = drawLine(armEndX, upperArmHeightY, armEndX, lowerArmEndY);
		add(lowerLeftArm);
	}
	
	private void drawRightArm() {
		double armStartX = getWidth() / 2 + UPPER_ARM_LENGTH/2 + HEAD_RADIUS;
		double armEndX = getWidth() / 2 + UPPER_ARM_LENGTH/2 + HEAD_RADIUS + UPPER_ARM_LENGTH;
		double upperArmHeightY = getHeight()/2 - BODY_LENGTH + ARM_OFFSET_FROM_HEAD;
	      
		GLine upperRightArm = drawLine(armStartX, upperArmHeightY, armEndX, upperArmHeightY);
		add(upperRightArm);
	      
		double lowerArmEndY = upperArmHeightY + LOWER_ARM_LENGTH;
		GLine lowerRightArm = drawLine(armEndX, upperArmHeightY, armEndX, lowerArmEndY);
		add(lowerRightArm);
	}
	
	private void drawLeftLeg() {
		double hipStartX = getWidth() / 2 + UPPER_ARM_LENGTH / 2 + HEAD_RADIUS;
		double hipEndX = hipStartX - HIP_WIDTH;
		double hipHeightY = getHeight() / 2;
	     
		GLine leftHip = drawLine(hipStartX, hipHeightY, hipEndX, hipHeightY);
		add(leftHip);
	     
		double leftLegY = hipHeightY + LEG_LENGTH;
		GLine leftLeg = drawLine(hipEndX, hipHeightY, hipEndX, leftLegY);
		add(leftLeg);
	}
	
	private void drawRightLeg() {
		double hipStartX = getWidth() / 2 + UPPER_ARM_LENGTH / 2 + HEAD_RADIUS;
		double hipEndX = hipStartX + HIP_WIDTH;
		double hipHeightY = getHeight() / 2;
        
		GLine leftHip = drawLine(hipStartX, hipHeightY, hipEndX, hipHeightY);
		add(leftHip);
        
		double leftLegEndY = hipHeightY + LEG_LENGTH;
		GLine leftLeg = drawLine(hipEndX, hipHeightY, hipEndX, leftLegEndY);
		add(leftLeg);
	}
	
	private void drawRightFoot() {
		double footStartX = getWidth() / 2 + UPPER_ARM_LENGTH / 2 + HEAD_RADIUS + HIP_WIDTH;
		double footEndX = footStartX + FOOT_LENGTH;
		double footHeightY = getHeight() / 2 + LEG_LENGTH;
        
		GLine rightFoot = drawLine(footStartX, footHeightY, footEndX, footHeightY);
		add(rightFoot);
	}
	
	private void drawLeftFoot() {
		double footStartX = getWidth() / 2 + UPPER_ARM_LENGTH / 2 + HEAD_RADIUS - HIP_WIDTH;
		double footEndX = footStartX - FOOT_LENGTH;
		double footHeightY = getHeight() / 2 + LEG_LENGTH;
	     
		GLine leftFoot = drawLine(footStartX, footHeightY, footEndX, footHeightY);
		add(leftFoot);
	}
}
