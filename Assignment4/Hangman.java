/*
 * File: Hangman.java
 * ------------------
 * This program will eventually play the Hangman game from
 * Assignment #4.
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

import java.awt.*;

public class Hangman extends ConsoleProgram {

	private HangmanLexicon hangmanWordsList = new HangmanLexicon();
	
	private RandomGenerator rgen = RandomGenerator.getInstance();
	
	private int remainingGuesses = 8;
	
	private String selectedHangmanWord;
	
	// The character input that will be guessed by the user will be traded here
	private char ch;
	
	// This represents the string that the user will be guessing, which becomes less and less hidden over time. 
	private String userWordToGuess;
	
	private HangmanCanvas canvas;
	
	public void init() {
		this.canvas = new HangmanCanvas();
		add(this.canvas);
	}
	
	public void run() {
		setUpGame();
		canvas.reset();
		canvas.displayWord(this.userWordToGuess);
		playGame();	
	}
    
	private void setUpGame() {
    	selectHangmanWord();
    	this.userWordToGuess = generateDashString();
    	println("Welcome to Hangman");
    	println("The word now looks like this:" + userWordToGuess);
    	println("You have " + remainingGuesses + " left.");
    	
    }
    
	private void playGame() {
    	while (remainingGuesses > 0 ) {
    		String userInput = readLine("Your guess: ");
    		
    		while (userInput.length() != 1) {
    			userInput = readLine("Only one letter at a time is allowed. Try again: ");
    			if (userInput.length() == 1) {
    				break;
    			}
    		}
    			
			this.ch = Character.toUpperCase(userInput.charAt(0));
			
			letterCheck(ch);
				if (userWordToGuess.equals(selectedHangmanWord)) {
					println("You guessed the word: " + selectedHangmanWord);
					println("You Win");
					break;
				}
					
			println("The word now looks like this: " + userWordToGuess);
			println("You have " + remainingGuesses + " left.");
    			
    	}
    		
    		if (remainingGuesses == 0) {
			println("You're completely hung.");
			println("The word was: " + selectedHangmanWord);
			println("You lose.");
    		}
    }
    
	private void selectHangmanWord() {
    	int randomWord = rgen.nextInt(0, hangmanWordsList.getWordCount() - 1);
    	selectedHangmanWord = hangmanWordsList.getWord(randomWord); 		
    }
    
	private String generateDashString() {
    	String result = "";
    	for (int i = 0; i < selectedHangmanWord.length(); i++) {
    		result += "-";
    	}
    		
    	return result;
    }
     
	private void letterCheck(char ch) {
		if(isCorrectGuess(this.ch)) {
			processCorrectGuess(this.ch);
			replaceDashAndDisplayWord(ch);
			
		} else {
			processIncorrectGuess(this.ch);
		}	
	}

	private boolean isCorrectGuess(char ch) {
		if(selectedHangmanWord.indexOf(ch) != -1) {
			return true;
		}
		
		return false;
	}
	
	private void processIncorrectGuess(char ch) {
		println("There are no " + ch + "'s in the word");
		canvas.noteIncorrectGuess(ch);
		remainingGuesses--;
		println("You have " + remainingGuesses + " guesses left.");
	}
	
	private void processCorrectGuess(char ch) {
		println("The guess is correct.");
	}
	
	private void replaceDashAndDisplayWord(char ch) {
		for (int i = 0; i < selectedHangmanWord.length(); i++) {
			if (ch == selectedHangmanWord.charAt(i)) {
				
				if (i == 0) {
					userWordToGuess = ch + userWordToGuess.substring(1);
				}
				
				if (i > 0) {
					userWordToGuess = userWordToGuess.substring(0, i) + ch + userWordToGuess.substring(i + 1);
				}
			}
		}
		
		canvas.displayWord(this.userWordToGuess);
	}
}
