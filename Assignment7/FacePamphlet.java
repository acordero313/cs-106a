/* 
 * File: FacePamphlet.java
 * -----------------------
 * When it is finished, this program will implement a basic social network
 * management system.
 */

import acm.program.*;
import acm.graphics.*;
import acm.util.*;
import java.awt.event.*;
import javax.swing.*;

public class FacePamphlet extends Program 
					implements FacePamphletConstants {
	
	private JTextField nameField;
	private JTextField changeStatusField;
	private JTextField changePictureField;
	private JTextField addFriendField;
	
	private JButton add;
	private JButton delete;
	private JButton lookup;
	private JButton changeStatus;
	private JButton changePicture;
	private JButton addFriend;
	
	private JLabel name;
	
	private FacePamphletProfile newUserProfile;
	private FacePamphletProfile currentUserProfile;
	private FacePamphletDatabase userInformation = new FacePamphletDatabase();
	
	private FacePamphletCanvas canvas = new FacePamphletCanvas();
	
	/**
	 * This method has the responsibility for initializing the 
	 * interactors in the application, and taking care of any other 
	 * initialization that needs to be performed.
	 */
	public void init() {
		name = drawLabel("Name");
		add(name, NORTH);
		
		nameField = drawTextField(TEXT_FIELD_SIZE);
		add(nameField, NORTH);
		nameField.addActionListener(this);
		
		add = createButton("Add");
		delete = createButton("Delete");
		lookup = createButton("Lookup");
		add(add, NORTH);
		add(delete, NORTH);
		add(lookup, NORTH);
				
		JLabel emptySpaceAfterChangeStatus = drawLabel(EMPTY_LABEL_TEXT);
		JLabel emptySpaceAfterChangePicture = drawLabel(EMPTY_LABEL_TEXT);
		
		changeStatusField = drawTextField(TEXT_FIELD_SIZE);
		changePictureField = drawTextField(TEXT_FIELD_SIZE);
		addFriendField = drawTextField(TEXT_FIELD_SIZE);
		
		changeStatus = createButton("Change Status");
		changePicture = createButton("Change Picture");
		addFriend = createButton("Add Friend");
		
		add(changeStatusField, WEST);
		changeStatusField.addActionListener(this);
		add(changeStatus, WEST);
		
		add(emptySpaceAfterChangeStatus, WEST);

		add(changePictureField, WEST);
		changePictureField.addActionListener(this);
		add(changePicture, WEST);
		
		add(emptySpaceAfterChangePicture, WEST);

		add(addFriendField, WEST);
		addFriendField.addActionListener(this);
		add(addFriend, WEST);
		
		addActionListeners();
		add(canvas);
    }
    
    /**
     * This class is responsible for detecting when the buttons are
     * clicked or interactors are used, so you will have to add code
     * to respond to these actions.
     */
	public void actionPerformed(ActionEvent e) {
		String enteredName = nameField.getText();
		
		if (!isValidNameEntry(enteredName)) {
			return;
		}
		
		if(isAddButtonClicked(e)) {
			
			if (userProfileExists(enteredName)) {
				displayUserProfile(enteredName);
				showErrorMessage("User: " + enteredName  
						+ " already exists, please enter another name");
				setAsCurrentUserProfile(enteredName);
				return;
			}
			
			createNewProfile(enteredName);
			displayUserProfile(enteredName);
			setAsCurrentUserProfile(enteredName);
		}
			
		if(isDeleteButtonClicked(e)) {
			clearCurrentUser();
			
			if(!userProfileExists(enteredName)) {
				showErrorMessage("This profile " + enteredName + " does not exist. ");
				return;
			} 
			profileDeleteConfirmationMessage(enteredName);
		}
		
		if(isLookupButtonClicked(e)) {
			clearCurrentUser();
			
			if (!userProfileExists(enteredName)) {
				showErrorMessage("This user: " + enteredName + " does not exist");
				return;
			}
			setAsCurrentUserProfile(enteredName);
			displayUserProfile(enteredName);
		}
	
		if(shouldChangeStatus(e)) {
			String userStatusEntry = this.changeStatusField.getText();
			
			if(this.currentUserProfile == null) {
				showErrorMessage("Please select a profile in order to update status.");
			} 
			updateUserStatus(userStatusEntry);	
		}
		
		if(shouldChangePicture(e)) {
			String filename = this.changePictureField.getText();
			
				if(this.currentUserProfile != null) {
					updateUserPicture(filename);
					canvas.showMessage("You have updated your new picture");
				}
		}
		
		if(shouldAddFriend(e)) {
			String addFriendName = this.addFriendField.getText();
			
			if(this.currentUserProfile != null) {
				showErrorMessage("The user you have entered does not "
						+ "exist and cannot be added as a friend");
			} 
			addUserFriend(addFriendName, enteredName);
		}
	}
	
	private void clearCurrentUser() {
		canvas.removeAll();
		this.currentUserProfile = null;
	}
	
	private GImage updateUserPicture(String filename) {
		FacePamphletProfile profile = this.userInformation.getProfile
				(this.currentUserProfile.getName());
		GImage image = null;
		
		try {
			
			image = new GImage (filename);
			profile.setImage(image);
			
		} catch (ErrorException ex) {
			
			showErrorMessage("File could not be found, please enter another file name.");
			image = null;
		}
		
		canvas.displayProfile(profile);
		return image;
	}
	
	private void addUserFriend(String friendName, String enteredName) {
		FacePamphletProfile userProfile = this.userInformation.getProfile
				(this.currentUserProfile.getName());
		
		if(this.userInformation.containsProfile(friendName)) {
			FacePamphletProfile userFriendProfile = this.userInformation.getProfile(friendName);
			
			if(this.currentUserProfile.getName().equals(friendName)) {
				showErrorMessage("You cannot add yourself as a friend.");
				return;
			}
			
			if(userProfile.addFriend(friendName)) {
				this.currentUserProfile.addFriend(friendName);
				userFriendProfile.addFriend(enteredName);
				canvas.displayProfile(this.currentUserProfile);
				canvas.showMessage(friendName + " has been added to your friends list.");
				
			} else {
				
				showErrorMessage(friendName + " has already been added as a friend");
			}
		}
	}
	
	private FacePamphletProfile getUserProfile() {
		FacePamphletProfile userProfile;
		userProfile = this.userInformation.getProfile(this.currentUserProfile.getName());
		return userProfile;
	}

	private void updateUserStatus(String statusEntry) {
		FacePamphletProfile userProfile = getUserProfile();
		userProfile.setStatus(statusEntry);
		canvas.displayProfile(userProfile);
		canvas.showMessage("Status updated to " + statusEntry);
	}
	
	private FacePamphletProfile setAsCurrentUserProfile(String name) {
		if (this.newUserProfile != this.currentUserProfile) {
			this.currentUserProfile = this.newUserProfile;
		}
		this.currentUserProfile = this.userInformation.getProfile(name);
		return this.currentUserProfile;
	}
	
	private void displayUserProfile(String name) {
		if (this.newUserProfile != null) {
			canvas.displayProfile(this.newUserProfile);
			canvas.showMessage("New profile created");
			
		} else if (this.currentUserProfile != null)
			canvas.displayProfile(this.currentUserProfile);
			canvas.showMessage("Now Showing: " + name);
	}
	
	private void profileDeleteConfirmationMessage(String name) {
		this.userInformation.deleteProfile(name);
		canvas.showMessage("Profile of " + name + " has been deleted");
	}
	
	private FacePamphletProfile createNewProfile(String name) {
		this.newUserProfile = new FacePamphletProfile(name);
		this.userInformation.addProfile(this.newUserProfile);	
		this.newUserProfile = this.userInformation.getProfile(name);	
		return this.newUserProfile;	
	}
	
	public void showErrorMessage(String errorMessage) {
		canvas.showMessage(errorMessage);
	}
	
	private boolean isValidNameEntry(String name) {		
		return name != "";
	}
	
	private boolean shouldAddFriend(ActionEvent e) {		
		return e.getActionCommand().equals("Add Friend") || e.getSource() == addFriendField;
	}
	
	private boolean shouldChangePicture(ActionEvent e) {
		return e.getActionCommand().equals("Change Picture") || e.getSource() == changePictureField;	
	}
	
	private boolean shouldChangeStatus(ActionEvent e) {
		return e.getActionCommand().equals("Change Status") || e.getSource() == changeStatusField;
	}
	
	private boolean isLookupButtonClicked(ActionEvent e) {
		return e.getActionCommand().equals("Lookup");
	}
	
	private boolean isDeleteButtonClicked(ActionEvent e) {
		return e.getActionCommand().equals("Delete");
	}
	
	private boolean isAddButtonClicked(ActionEvent e) {
		return e.getActionCommand().equals("Add");
	}
	
	private boolean userProfileExists(String name) {
		return this.userInformation.getProfile(name) != null;
	}
    
    private JButton createButton(String buttonName) {
    		JButton button = new JButton(buttonName);
    		return button;
    }
    
    private JTextField drawTextField(int textFieldSize) {
    		JTextField textField = new JTextField(textFieldSize);
    		return textField;
    }
    
    private JLabel drawLabel(String label) {
    		JLabel labelName = new JLabel(label);
    		return labelName;
    }
}
